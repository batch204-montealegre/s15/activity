console.log("Hello World");

console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let firstName = "Roxanne";
		console.log(firstName);
	let lastName = "Montealegre";
		console.log(lastName);
	let age = 30;
		console.log(age);
	let hobbies = ["Coding", "Netflix", "Playing online games"];
		console.log(hobbies);

	let workAddress = {
			houseNumber : 24 + "th" + " " + "floor" + " " + "Net Park Building",
			street: "5th ave",
			city: "Taguig City",
			state: "Metro Manila",
			
	};
	console.log(workAddress);


	let name = "Steve Rogers";
	console.log("My full name is" + " " +name);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor", "Natasha","Clint","Nick"];
	console.log("My Friends are: " +friends)
	console.log(friends);

	let profile = {

		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
		
		};
	console.log("My Full Profile:\n"  

		+ "user Name:" + " " +profile.userName 
		+ "\nFull Name:" + " " +profile.fullName
		+ "\nAge:" + " " +profile.age
		);

	console.log(profile);

	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);



